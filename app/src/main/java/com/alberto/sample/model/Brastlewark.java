package com.alberto.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Model class to represent a Brastlewark object
 */
public class Brastlewark implements Parcelable {

    @SerializedName("Brastlewark") private List<Gnome> gnomeList;

    /**
     * Return list of gnomes
     * @return List<Gnome>
     */
    public List<Gnome> getGnomeList() {
        return gnomeList;
    }

    protected Brastlewark(Parcel in) {
        if (in.readByte() == 0x01) {
            gnomeList = new ArrayList<Gnome>();
            in.readList(gnomeList, Gnome.class.getClassLoader());
        } else {
            gnomeList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (gnomeList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(gnomeList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Brastlewark> CREATOR = new Parcelable.Creator<Brastlewark>() {
        @Override
        public Brastlewark createFromParcel(Parcel in) {
            return new Brastlewark(in);
        }

        @Override
        public Brastlewark[] newArray(int size) {
            return new Brastlewark[size];
        }
    };
}