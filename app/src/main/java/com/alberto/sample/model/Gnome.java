package com.alberto.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to represent a Gnome object
 */
public class Gnome implements Parcelable {

    private Integer id;
    private String name;
    private String thumbnail;
    private Integer age;
    private Double weight;
    private Double height;
    private String hair_color;
    private List<String> professions = new ArrayList<>();
    private List<String> friends = new ArrayList<>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     *
     * @param thumbnail
     * The thumbnail
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     *
     * @return
     * The age
     */
    public Integer getAge() {
        return age;
    }

    /**
     *
     * @param age
     * The age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     *
     * @return
     * The weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     *
     * @param weight
     * The weight
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    /**
     *
     * @return
     * The height
     */
    public Double getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */
    public void setHeight(Double height) {
        this.height = height;
    }

    /**
     *
     * @return
     * The hair_color
     */
    public String getHair_color() {
        return hair_color;
    }

    /**
     *
     * @param hair_color
     * The hair_color
     */
    public void setHair_color(String hair_color) {
        this.hair_color = hair_color;
    }

    /**
     *
     * @return
     * The professions
     */
    public List<String> getProfessions() {
        return professions;
    }

    /**
     *
     * @param professions
     * The professions
     */
    public void setProfessions(List<String> professions) {
        this.professions = professions;
    }

    /**
     *
     * @return
     * The friends
     */
    public List<String> getFriends() {
        return friends;
    }

    /**
     *
     * @param friends
     * The friends
     */
    public void setFriends(List<String> friends) {
        this.friends = friends;
    }


    protected Gnome(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        name = in.readString();
        thumbnail = in.readString();
        age = in.readByte() == 0x00 ? null : in.readInt();
        weight = in.readByte() == 0x00 ? null : in.readDouble();
        height = in.readByte() == 0x00 ? null : in.readDouble();
        hair_color = in.readString();
        if (in.readByte() == 0x01) {
            professions = new ArrayList<String>();
            in.readList(professions, String.class.getClassLoader());
        } else {
            professions = null;
        }
        if (in.readByte() == 0x01) {
            friends = new ArrayList<String>();
            in.readList(friends, String.class.getClassLoader());
        } else {
            friends = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(thumbnail);
        if (age == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(age);
        }
        if (weight == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(weight);
        }
        if (height == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(height);
        }
        dest.writeString(hair_color);
        if (professions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(professions);
        }
        if (friends == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(friends);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Gnome> CREATOR = new Parcelable.Creator<Gnome>() {
        @Override
        public Gnome createFromParcel(Parcel in) {
            return new Gnome(in);
        }

        @Override
        public Gnome[] newArray(int size) {
            return new Gnome[size];
        }
    };
}