package com.alberto.sample.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alberto.sample.R;
import com.alberto.sample.model.Gnome;
import com.alberto.sample.presenter.MainPresenter;

import java.util.List;

/**
 * Activity with an AutoCompleteTextView containing a list of the available countries
 */
public class MainActivity extends AppCompatActivity implements MainView {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private GnomeAdapter gnomeAdapter;
    private TextView errorText;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        errorText = (TextView) findViewById(R.id.error_text);
        recyclerView = (RecyclerView) findViewById(R.id.gnomes_recyclerview);

        initRecyclerView();

        mainPresenter = new MainPresenter();
        mainPresenter.attachView(this);
        mainPresenter.getGnomes();
    }

    private void initRecyclerView() {
        gnomeAdapter = new GnomeAdapter(this);
        recyclerView.setAdapter(gnomeAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                gnomeAdapter.filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        mainPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void showGnomes(List<Gnome> gnomes) {
        // Hide progress layout and update adapter to display data
        progressBar.setVisibility(View.GONE);
        gnomeAdapter.setData(gnomes);
        gnomeAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        errorText.setVisibility(View.VISIBLE);
        errorText.setText(getString(R.string.error, errorMessage));
    }

    @Override
    public Context getContext() {
        return this;
    }
}
