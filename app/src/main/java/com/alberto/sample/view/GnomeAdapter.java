package com.alberto.sample.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberto.sample.R;
import com.alberto.sample.model.Gnome;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


/**
 * Adapter for the RecyclerView that displays the list of gnomes
 */
public class GnomeAdapter extends RecyclerView.Adapter<GnomeAdapter.GnomeViewHolder> {

    private final Context context;
    private List<Gnome> gnomes;
    private List<Gnome> initialGnomeList = new ArrayList<>();

    // Constructor
    public GnomeAdapter(Context context) {
        this.context = context;
        this.gnomes = new ArrayList<>();
    }

    @Override
    public GnomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(context).inflate(R.layout.gnome_row, parent, false);
        return new GnomeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GnomeViewHolder holder, int position) {
        final Gnome gnome = gnomes.get(position);
        holder.bind(gnome);
    }

    @Override
    public int getItemCount() {
        return gnomes.size();
    }

    // Sets the list of gnomes
    public void setData(List<Gnome> gnomes) {
        this.gnomes = new ArrayList<>(gnomes);

        // Keep a copy of the initial list
        initialGnomeList.addAll(gnomes);
    }

    // Filters the list of gnomes according to the received text
    public void filter(String text) {
        if (text.isEmpty()) {
            // If text is empty, all the initial gnomes should be displayed
            gnomes.clear();
            gnomes.addAll(initialGnomeList);
        } else {
            List<Gnome> result = new ArrayList<>();
            text = text.toLowerCase();

            // Check which gnomes of the initial list contains the received text
            for (Gnome item: initialGnomeList){
                if(item.getName().toLowerCase().contains(text)){
                    result.add(item);
                }
            }
            gnomes.clear();

            // Set the filtered results
            gnomes.addAll(result);
        }

        // Notify the adapter that the dataset has changed
        notifyDataSetChanged();
    }

    /**
     * ViewHolder for a Gnome object
     */
    class GnomeViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final ImageView thumbnail;

        // Constructor
        public GnomeViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
        }

        // Binds Gnome object
        public void bind(final Gnome gnome) {
            name.setText(gnome.getName());

            // Use Glide to load remote image
            Glide.with(context).load(gnome.getThumbnail()).into(thumbnail);

            // If item is clicked, launch new activity to display more info about the gnome
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Put the Gnome object in the Intent and start GnomeActivity
                    Intent intent = new Intent(context, GnomeActivity.class);
                    intent.putExtra(Gnome.class.getName(), gnome);
                    context.startActivity(intent);
                }
            });
        }
    }
}
