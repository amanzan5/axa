package com.alberto.sample.view;

import com.alberto.sample.model.Gnome;

import java.util.List;

/**
 * Interface that represents MVP's View for the MainActivity
 */
public interface MainView extends View {

    // Displays list of gnomes received from the presenter
    void showGnomes(List<Gnome> gnomes);

    // Displays error received from the presenter
    void showError(String errorMessage);
}
