package com.alberto.sample.view;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alberto.sample.R;
import com.alberto.sample.model.Gnome;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

/**
 * Activity to display details of a Gnome
 */
public class GnomeActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView gnomeImage;
    private ProgressBar loadingGnomeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Slide in from right animation for this activity, slide out to left for the previous one
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

        setContentView(R.layout.activity_gnome_detail);

        init();
    }

    private void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        gnomeImage = (ImageView) findViewById(R.id.gnome_image);
        loadingGnomeImage = (ProgressBar) findViewById(R.id.loading_image);

        loadGnomeInfo();
    }

    /**
     * Clicks on the back button will show a slide in/out animation
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    /**
     * Clicks on the Toolbar's back button will show a slide in/out animation
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }

    // Displays the info about the gnome
    private void loadGnomeInfo() {
        // Get the gnome that was sent by the MainActivity
        final Gnome gnome = getIntent().getExtras().getParcelable(Gnome.class.getName());

        if (gnome != null) {
            // Show name of the gnome in the collapsing toolbar
            collapsingToolbarLayout.setTitle(gnome.getName());

            // Load gnome image using Glide
            Glide.with(this)
                    .load(gnome.getThumbnail())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Log.e("GlideError", "Glide failed: " + e.getMessage());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            loadingGnomeImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(gnomeImage);

            // Show more info about the gnome
            setText((TextView) findViewById(R.id.name), gnome.getName());
            setText((TextView) findViewById(R.id.age), Integer.toString(gnome.getAge()));
            setText((TextView) findViewById(R.id.height), Double.toString(gnome.getHeight()));
            setText((TextView) findViewById(R.id.weight), Double.toString(gnome.getWeight()));
            setText((TextView) findViewById(R.id.hair_color), gnome.getHair_color());
            setText((TextView) findViewById(R.id.professions), gnome.getProfessions());
            setText((TextView) findViewById(R.id.friends), gnome.getFriends());
        }
    }

    // Displays the received text in the received textView
    private void setText(TextView text, String info) {
        text.setText(info);
    }

    // Displays the received List of Strings in the received textView
    private void setText(TextView text, List<String> list) {
        String formattedString = "";

        for (int i = 0; i < list.size(); i++) {
            formattedString += list.get(i);
            // Add newline except for the last item
            if (i != list.size() - 1) {
                formattedString += "\n";
            }
        }

        text.setText(formattedString);
    }
}
