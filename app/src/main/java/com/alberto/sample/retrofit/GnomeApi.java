package com.alberto.sample.retrofit;

import com.alberto.sample.model.Brastlewark;

import retrofit2.http.GET;
import rx.Single;

/**
 * Brastlewark interface for Retrofit
 */
public interface GnomeApi {

    @GET("data.json")
    Single<Brastlewark> getGnomes();
}
