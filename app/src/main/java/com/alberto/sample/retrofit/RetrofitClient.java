package com.alberto.sample.retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Singleton object containing all Retrofit objects needed within the app
 */
public class RetrofitClient {

    // URL to get content
    public static final String GNOMES_URL = "https://raw.githubusercontent.com/AXA-GROUP-SOLUTIONS/mobilefactory-test/master/";

    private static RetrofitClient sRetrofitClient;
    private GnomeApi gnomeApi;

    /**
     * Restrict access to constructor to assure Singleton pattern usage
     */
    private RetrofitClient() {}

    /**
     * Singleton call to get a static instance
     * @return RetrofitClient
     */
    public static synchronized RetrofitClient getInstance() {
        if (sRetrofitClient == null) {
            sRetrofitClient = new RetrofitClient();
        }

        return sRetrofitClient;
    }

    /**
     * Creates gnomeApi if it's not already created
     * @return GnomeApi
     */
    public GnomeApi getGnomeApi() {
        if (gnomeApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GNOMES_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            gnomeApi = retrofit.create(GnomeApi.class);
        }

        return gnomeApi;
    }
}
