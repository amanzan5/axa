package com.alberto.sample.presenter;

import com.alberto.sample.model.Brastlewark;
import com.alberto.sample.retrofit.RetrofitClient;
import com.alberto.sample.view.MainView;

import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter for the MainActivity
 */
public class MainPresenter implements Presenter<MainView> {

    private MainView mainView;
    private Subscription subscription;

    @Override
    public void attachView(MainView view) {
        // Keep a copy of the view where we will present results
        mainView = view;
    }

    @Override
    public void detachView() {
        // Release view and unsubscribe if needed
        mainView = null;
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    /**
     * Makes API call to get list of Gnomes using Retrofit + RxJava
     */
    public void getGnomes() {
        subscription = RetrofitClient.getInstance().getGnomeApi().getGnomes()
                .observeOn(AndroidSchedulers.mainThread()) // observe on UI thread
                .subscribeOn(Schedulers.io()) // make the call on a background thread
                .subscribe(new SingleSubscriber<Brastlewark>() {
                    @Override
                    public void onSuccess(Brastlewark value) {
                        mainView.showGnomes(value.getGnomeList());
                    }

                    @Override
                    public void onError(Throwable error) {
                        mainView.showError(error.getMessage());
                    }
                });
    }
}
