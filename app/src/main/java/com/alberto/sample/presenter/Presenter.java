package com.alberto.sample.presenter;

/**
 * Interface that represents the MVP's Presenter. Base for all future presenters within the app
 * @param <V>
 */
public interface Presenter<V> {

    // Associates a View to the Presenter
    void attachView(V view);

    // Releases the view from the Presenter
    void detachView();
}
